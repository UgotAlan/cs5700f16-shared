﻿using System;
using System.Linq;

namespace Triangles
{
    public class Triangle
    {
        public double?[] Sides { get; set; }

        public bool IsValid
        {
            get
            {
                if (Sides?.Length != 3) return false;

                foreach (double? side in Sides)
                    if (side == null || side <= 0) return false;

                if ((Sides[0] >= Sides[1] + Sides[2]) ||
                    (Sides[1] >= Sides[0] + Sides[2]) ||
                    (Sides[2] >= Sides[0] + Sides[1])) return false;

                return true;
            }
        }

        public double? ComputeArea()
        {
            if (!IsValid) return null;

            double halfOfPerimeter = (double) Sides.Sum()/2;

            return Math.Sqrt(halfOfPerimeter *
                                    (halfOfPerimeter - Sides[0].Value)*
                                    (halfOfPerimeter - Sides[1].Value)*
                                    (halfOfPerimeter - Sides[2].Value));
        }
    }
}
