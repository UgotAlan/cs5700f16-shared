﻿using System;
using System.Collections.Generic;
using System.Resources;
using System.Text.RegularExpressions;
using Microsoft.VisualStudio.TestTools.UnitTesting;

using Triangles;

namespace TrianglesTesting
{
    [TestClass]
    public class TriangleTester
    {
        [TestMethod]
        public void Triangle_CheckValidSimpleScalene()
        {
            Triangle t = new Triangle() {Sides = new double?[] {3.0, 4.0, 5.0}};
            double? area = t.ComputeArea();
            Assert.IsNotNull(area);
            Assert.AreEqual(6.0, area);
        }

        [TestMethod]
        public void Triangle_CheckInvalidWithFourSide()
        {
            Triangle t = new Triangle() { Sides = new double?[] { 3.0, 4.0, 5.0, 6.0 } };

            double? area = t.ComputeArea();

            Assert.IsNull(area);
        }

        [TestMethod]
        public void Triangle_TestLotsOfCases()
        {
            CheckListOfTestCases(GetEquilateralTriangles());
            CheckListOfTestCases(GetIsoscelesTriangles());
            CheckListOfTestCases(GetScaleneTriangles());
            CheckListOfTestCases(GetBadCases());
        }

        private static void CheckListOfTestCases(List<TestCaseData> testCases)
        {
            foreach (TestCaseData testCase in testCases)
            {
                // Setup
                Triangle t = new Triangle() {Sides = testCase.Sides};
                double? area = t.ComputeArea();

                // Make sure the area is as expected
                if (testCase.ExceptedArea == null)
                    Assert.IsNull(area);
                else
                {
                    Assert.IsNotNull(area);
                    Assert.AreEqual(Math.Round(testCase.ExceptedArea.Value, 3), Math.Round(area.Value, 3));
                }

                // Make sure the triangle didn't change
                Assert.AreEqual(testCase.Sides?.Length, t.Sides?.Length);
                for (int i = 0; i < testCase.Sides?.Length; i++)
                    Assert.AreEqual(testCase.Sides[i], t.Sides[i]);
            }
        }

        private List<TestCaseData> GetEquilateralTriangles()
        {
            return new List<TestCaseData>
            {
                new TestCaseData() { Sides = new double?[] {2.0, 2.0, 2.0}, ExceptedArea = 1.7321 },
                new TestCaseData() { Sides = new double?[] {3.5, 3.5, 3.5}, ExceptedArea = 5.3044 },
                new TestCaseData() { Sides = new double?[] {0.5, 0.5, 0.5}, ExceptedArea = 0.1083 }
            };
        }

        private List<TestCaseData> GetIsoscelesTriangles()
        {
            return new List<TestCaseData>
            {
                new TestCaseData() { Sides = new double?[] {4.0, 4.0, 2.0}, ExceptedArea = 3.8730 },
                new TestCaseData() { Sides = new double?[] {2.0, 4.0, 4.0}, ExceptedArea = 3.8730 },
                new TestCaseData() { Sides = new double?[] {4.0, 2.0, 4.0}, ExceptedArea = 3.8730 },
                new TestCaseData() { Sides = new double?[] {1.5, 1.5, 0.5}, ExceptedArea = 0.3698 },
                new TestCaseData() { Sides = new double?[] {0.5, 1.5, 1.5}, ExceptedArea = 0.3698 },
                new TestCaseData() { Sides = new double?[] {1.5, 0.5, 1.5}, ExceptedArea = 0.3698 }
            };
        }

        private List<TestCaseData> GetScaleneTriangles()
        {
            return new List<TestCaseData>
            {
                new TestCaseData() { Sides = new double?[] {5.0, 6.0, 7.0}, ExceptedArea = 14.6969 },
                new TestCaseData() { Sides = new double?[] {7.0, 5.0, 6.0}, ExceptedArea = 14.6969 },
                new TestCaseData() { Sides = new double?[] {6.0, 7.0, 5.0}, ExceptedArea = 14.6969 },
                new TestCaseData() { Sides = new double?[] {3.3, 6.1, 8.5}, ExceptedArea = 8.0531 },
                new TestCaseData() { Sides = new double?[] {8.5, 3.3, 6.1}, ExceptedArea = 8.0531 },
                new TestCaseData() { Sides = new double?[] {6.1, 8.5, 3.3}, ExceptedArea = 8.0531 }
            };
        }

        private List<TestCaseData> GetBadCases()
        {
            return new List<TestCaseData>
            {
                new TestCaseData() { Sides = null, ExceptedArea = null },
                new TestCaseData() { Sides = new double?[] {}, ExceptedArea = null },
                new TestCaseData() { Sides = new double?[] {0.0, 0.0, 0.0}, ExceptedArea = null },
                new TestCaseData() { Sides = new double?[] {0.0, 1.0, 2.0}, ExceptedArea = null },
                new TestCaseData() { Sides = new double?[] {1.0, 0.0, 2.0}, ExceptedArea = null },
                new TestCaseData() { Sides = new double?[] {1.0, 2.0, 0.0}, ExceptedArea = null },
                new TestCaseData() { Sides = new double?[] {6.0, 7.0}, ExceptedArea = null },
                new TestCaseData() { Sides = new double?[] {3.3, 6.1, 8.5, 9.0}, ExceptedArea = null },
                new TestCaseData() { Sides = new double?[] {8.5, 3.3, -6.1}, ExceptedArea = null },
                new TestCaseData() { Sides = new double?[] {-8.5, 3.3, 6.1}, ExceptedArea = null },
                new TestCaseData() { Sides = new double?[] {8.5, -3.3, 6.1}, ExceptedArea = null },
                new TestCaseData() { Sides = new double?[] {6.1, 80.0, 3.3}, ExceptedArea = null }
            };
        }

        public class TestCaseData
        {
            public double?[] Sides { get; set; }
            public double? ExceptedArea { get; set; }
        }
    }
}
